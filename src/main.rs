use std::io::{self, BufRead};
use std::fmt;

#[derive(Debug)]
enum Command {
    Insert { age: i32, name: String },
    Erase { age: i32, name: String },
    Contains { age: i32, name: String },
    Print,
    Reset,
    Exit,
    Error(String),
}

#[derive(Debug, PartialEq)]
struct TreeNode {
    age: i32,
    name: String,
    left: Option<Box<TreeNode>>,
    right: Option<Box<TreeNode>>,
}

impl TreeNode {
    fn new(age: i32, name: String) -> Self {
        TreeNode {
            age,
            name,
            left: None,
            right: None,
        }
    }
}

#[derive(Debug)]
struct SortedContainer {
    root: Option<Box<TreeNode>>,
}

impl SortedContainer {
    fn new() -> Self {
        SortedContainer { root: None }
    }

    fn insert(&mut self, age: i32, name: String) {
        let new_node = Box::new(TreeNode::new(age, name));
        let current_root = self.root.take();
        self.root = Some(self.insert_recursive(current_root, new_node));
    }

    fn insert_recursive(
        &mut self,
        current: Option<Box<TreeNode>>,
        new_node: Box<TreeNode>,
    ) -> Box<TreeNode> {
        match current {
            Some(mut node) => {
                if new_node.age <= node.age {
                    node.left = Some(self.insert_recursive(node.left.take(), new_node));
                } else {
                    node.right = Some(self.insert_recursive(node.right.take(), new_node));
                }
                node
            }
            None => new_node,
        }
    }

    fn contains(&self, age: i32, name: &str) -> bool {
        self.contains_recursive(self.root.as_ref(), age, name)
    }

    fn contains_recursive(&self, current: Option<&Box<TreeNode>>, age: i32, name: &str) -> bool {
        match current {
            Some(node) => {
                if age == node.age && name == &node.name {
                    true
                } else if age > node.age {
                    self.contains_recursive(node.right.as_ref(), age, name)
                } else {
                    self.contains_recursive(node.left.as_ref(), age, name)
                }
            }
            None => false,
        }
    }

    fn erase(&mut self, age: i32, name: &str) {
        let current_root = self.root.take();
        self.root = self.erase_recursive(current_root, age, name);
    }

    fn erase_recursive(
        &mut self,
        current: Option<Box<TreeNode>>,
        age: i32,
        name: &str,
    ) -> Option<Box<TreeNode>> {
        match current {
            Some(mut node) => {
                if age == node.age && name == &node.name {
                    if node.left.is_none() {
                        return node.right.take();
                    } else if node.right.is_none() {
                        return node.left.take();
                    } else {
                        let mut right_child = node.right.take();
                        let successor = self.find_min(right_child.as_mut());
                        node.age = successor.age;
                        node.name = std::mem::replace(&mut successor.name, String::new());
                        node.right = self.erase_recursive(node.right.take(), successor.age, &successor.name);
                    }
                } else if age > node.age {
                    node.right = self.erase_recursive(node.right.take(), age, name);
                } else {
                    node.left = self.erase_recursive(node.left.take(), age, name);
                }
                Some(node)
            }
            None => None,
        }
    }
    
    
    fn find_min<'a>(&mut self, current: Option<&'a mut Box<TreeNode>>) -> &'a mut TreeNode {
        let mut current = current.expect("Cannot find minimum in an empty tree");
        while let Some(ref mut left) = current.left {
            current = left;
        }
        &mut *current
    }

    fn print(&self) {
        println!("{}", self);
    }
}

impl fmt::Display for SortedContainer {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.display_recursive(self.root.as_ref()))
    }
}

impl SortedContainer {
    fn display_recursive(&self, current: Option<&Box<TreeNode>>) -> String {
        match current {
            Some(node) => {
                format!(
                    "[{{\"{}\":\"{}\"}},{},{}]",
                    node.age,
                    node.name,
                    self.display_recursive(node.left.as_ref()),
                    self.display_recursive(node.right.as_ref())
                )
            }
            None => "null".to_string(),
        }
    }
}

fn parse_command(input: String) -> Command {
    let command_items: Vec<&str> = input.split_whitespace().collect();
    if command_items.is_empty() {
        Command::Error("invalid command (empty line)".to_string())
    } else {
        match (command_items[0], command_items.len()) {
            ("p", 1) => Command::Print,
            ("q", 1) => Command::Exit,
            ("x", 1) => Command::Reset,
            ("i", 3) => {
                if let Ok(age) = command_items[1].parse::<i32>() {
                    Command::Insert {
                        age,
                        name: command_items[2].to_string(),
                    }
                } else {
                    Command::Error("unable to parse int (age).".to_string())
                }
            }
            ("e", 3) => {
                if let Ok(age) = command_items[1].parse::<i32>() {
                    Command::Erase {
                        age,
                        name: command_items[2].to_string(),
                    }
                } else {
                    Command::Error("unable to parse int (age).".to_string())
                }
            }
            ("c", 3) => {
                if let Ok(age) = command_items[1].parse::<i32>() {
                    Command::Contains {
                        age,
                        name: command_items[2].to_string(),
                    }
                } else {
                    Command::Error("unable to parse int (age).".to_string())
                }
            }
            (_, _) => Command::Error("invalid command.".to_string()),
        }
    }
}

fn command_loop(br: &mut dyn BufRead) {
    let mut container = SortedContainer::new();

    loop {
        let mut input = String::new();

        match br.read_line(&mut input) {
            Ok(0) => {
                // End of file
                break;
            }
            Ok(_) => {
                match parse_command(input) {
                    Command::Insert { age, name } => {
                        container.insert(age, name);
                    }
                    Command::Erase { age, name } => {
                        container.erase(age, &name);
                    }
                    Command::Contains { age, name } => {
                        if container.contains(age, &name) {
                            println!("Contains: Age: {}, Name: {}", age, name);
                        } else {
                            println!("Does not contain: Age: {}, Name: {}", age, name);
                        }
                    }
                    Command::Print => {
                        container.print();
                    }
                    Command::Reset => {
                        container = SortedContainer::new();
                    }
                    Command::Exit => {
                        break;
                    }
                    Command::Error(error) => {
                        eprintln!("Error: {}", error);
                    }
                }
            }
            Err(error) => eprintln!("Error: {}", error),
        }
    }
}

fn main() {
    let stdin = io::stdin();
    let mut handle = stdin.lock();
    command_loop(&mut handle);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_insert_contains() {
        let mut container = SortedContainer::new();
        container.insert(25, "Alice".to_string());
        container.insert(30, "Bob".to_string());
        container.insert(20, "Charlie".to_string());

        assert!(container.contains(25, "Alice"));
        assert!(container.contains(30, "Bob"));
        assert!(container.contains(20, "Charlie"));
        assert!(!container.contains(40, "David"));
    }

    #[test]
    fn test_erase() {
        let mut container = SortedContainer::new();
        container.insert(25, "Alice".to_string());
        container.insert(30, "Bob".to_string());
        container.insert(20, "Charlie".to_string());

        container.erase(30, "Bob");

        assert!(!container.contains(30, "Bob"));
        assert!(container.contains(25, "Alice"));
        assert!(container.contains(20, "Charlie"));
    }

    #[test]
    fn test_display_recursive() {
        let mut container = SortedContainer::new();

        // Test displaying an empty tree
        assert_eq!(container.to_string(), "null");

        // Test displaying a non-empty tree
        container.insert(32, "Erik".to_string());
        container.insert(20, "Alice".to_string());
        container.insert(25, "Bob".to_string());

        assert_eq!(
            container.to_string(),
            "[{\"32\":\"Erik\"},[{\"20\":\"Alice\"},null,[{\"25\":\"Bob\"},null,null]],null]"
        );
    }

    #[test]
fn test_reset_command() {
    // Create a SortedContainer
    let mut container = SortedContainer::new();

    // Insert some elements
    container.insert(25, "Alice".to_string());
    container.insert(30, "Bob".to_string());
    container.insert(20, "Charlie".to_string());

    // Simulate the "x" command
    container = SortedContainer::new(); // This line simulates the effect of "x" command

    // Check that the container is empty
    assert_eq!(container.to_string(), "null");
    }      
    
    #[test]
    fn stress_insert_test() {
        let mut container = SortedContainer::new();
        for i in 1..=10000 {
            container.insert(i, "User".to_string() + &i.to_string());
        }
        
        for i in 1..=10000 {
            container.erase(i, &("User".to_string() + &i.to_string()));
        }
    }

    fn stress_check_tree_order_test_recursive(node:Option<Box<TreeNode>>){

        match node {
            Some(boxed_node) => {

                let unboxed_node = *boxed_node;

                match unboxed_node.left {
                    Some(ref unboxed_left_node) => {

                        assert!(unboxed_node.age > unboxed_left_node.age);
                        stress_check_tree_order_test_recursive(unboxed_node.left);

                    }
                    _ => {
                       
                    }
                }

                match unboxed_node.right {
                    Some(ref unboxed_right_node) => {

                        assert!(unboxed_node.age < unboxed_right_node.age);
                        stress_check_tree_order_test_recursive(unboxed_node.right);
                        
                    }
                    _ => {
                        
                    }
                }
            }
            _ => {
                
            }
        }
    }

    #[test]
    fn stress_check_tree_order_test() {
        let mut container = SortedContainer::new();
        for i in 1..=10000 {
            container.insert(i, "User".to_string() + &i.to_string());
        }
        
        stress_check_tree_order_test_recursive(container.root);
        
    }

    #[test]
    fn test_erase_middle_element() {
        let mut container = SortedContainer::new();
        container.insert(25, "Alice".to_string());
        container.insert(30, "Bob".to_string());
        container.insert(27, "Charlie".to_string());

        // Check the printed output after initial insertions
        let expected_output_insert = "[{\"25\":\"Alice\"},null,[{\"30\":\"Bob\"},[{\"27\":\"Charlie\"},null,null],null]]";
        assert_eq!(container.to_string(), expected_output_insert);

        // Erase the middle element
        container.erase(30, "Bob");

        // Check the printed output after erasing the middle element
        let expected_output_erase = "[{\"25\":\"Alice\"},null,[{\"27\":\"Charlie\"},null,null]]";
        assert_eq!(container.to_string(), expected_output_erase);

        // Insert more elements
        container.insert(22, "David".to_string());
        container.insert(28, "Eve".to_string());

        // Check the printed output after additional insertions
        let expected_output_insert_more = "[{\"25\":\"Alice\"},[{\"22\":\"David\"},null,null],[{\"27\":\"Charlie\"},null,[{\"28\":\"Eve\"},null,null]]]";
        assert_eq!(container.to_string(), expected_output_insert_more);
    }
   
}